用世界上最好的语言python编写的Java代码生成器，私人订制，模版任意设置，使用此代码生成器10分钟可以迅速完成增删改查全部功能，提供api接口。

本项目为Spring + Mybatis + mysql + dubbo代码生成器，但只要增加模版便可以完成你的私人订制代码生成器，不限语言。

本项目已包含以下模版：实体类模版，service服务类接口模版，service服务类实现模版，mapper数据库接口模版，mapper xml配置文件模版，dubbo provider和customer模版，Controller模版。

